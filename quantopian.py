"""
Парный трейдинг. Дневной таймфрейм. Сигналы по пересечению скользящих.

# 2016: EQY, T; DIA, SLB; E, MMP;
# 2015: DRE, O; CIT, STT; TLT, VNQ; PNR, FDX;
# 2014-2015: DRE, O; H, MMP; CMS, SPG;

# 2014: SMH, SSO

"""
import talib
import pandas as pd
import numpy as np
from statsmodels.tsa.stattools import adfuller


# подготовка теста
def initialize(context):
    # пара
    context.pair = [
        symbol('TLT'),
        symbol('VNQ')
    ]

    schedule_function(trade_zscore, date_rules.every_day(), time_rules.market_open(hours=1))

# ежедневная проверка сигналов
def trade_zscore(context, data):
    stocks = context.pair

    can_trade = data.can_trade(stocks)

    # выходим, если не можем торговать одной из акций
    for stock in stocks:
        if not can_trade[stock]:
            log.warn("Can't trade " + stock.symbol)
            return

    # загрузка исторических данных
    hist = data.history(stocks, ['open', 'high', 'low', 'close'], 500, '1d')

    # переводим в относительные значения
    check_length = 120
    performance = [
        get_performance(hist['close'][stocks[0]][-check_length:].as_matrix()),
        get_performance(hist['close'][stocks[1]][-check_length:].as_matrix())
    ]

    spread_performance = pd.Series(performance[0] - performance[1])
    spread_price = hist['close'][stocks[0]][-check_length:] - hist['close'][stocks[1]][-check_length:]

    spread = spread_price
    spread = spread_performance
    zscore = z_score_ma(spread, short=5, long=50)  # by MA cross

    # сигнал
    z = zscore.iloc[-1]

    if len(spread[-check_length:].dropna()) < check_length:
        log.warn("Can't trade with short length")
        return

    # проверка стационарности
    result = adfuller(spread.iloc[-check_length:])
    score, pvalue, crit = result[0], result[1], result[4]
    coint = score < crit['5%']
    #coint = True  # проверка отключена

    if not coint:
        # если пропала стационарность, закрываемся
        order_target_percent(stocks[0], 0)
        order_target_percent(stocks[1], 0)
    elif z > 1:
        # если опережает акция А
        order_target_percent(stocks[0], -1)
        order_target_percent(stocks[1], 1)
    elif z < -1:
        # если опережает акция Б
        order_target_percent(stocks[0], 1)
        order_target_percent(stocks[1], -1)
    elif abs(z) < 0.05:
        # рядом с нулем закрываем позицию
        order_target_percent(stocks[0], 0)
        order_target_percent(stocks[1], 0)

    # собираем историю значений
    sign = abs(z) / z if z else 0
    record(**{
            'zscore': z if abs(z) <= 2.05 else sign*2.05,
            'z-perf': zscore_std(spread_performance).iloc[-1],
            'z-price': zscore_std(spread_price).iloc[-1],
            stocks[0].symbol: context.portfolio.positions[stocks[0]].amount,
            stocks[1].symbol: context.portfolio.positions[stocks[1]].amount
          })

def zscore_std(series):
    return (series - series.mean()) / np.std(series)

def z_score_ma(spread, short=10, long=60):
    # Получаем разницу цен двух наборов данных
    difference = spread
    difference.name = 'diff'

    # Получаем 10-дневную скользящую среднюю от разницы
    diff_mavg_short = difference.rolling(window=short, center=False).mean()
    diff_mavg_short.name = 'diff {0}d mavg'.format(short)

    # Получаем 60-дневную скользящую среднюю от разницы
    diff_mavg_long = difference.rolling(window=long, center=False).mean()
    diff_mavg_long.name = 'diff {0}d mavg'.format(long)

    # Получаем скользящее 60-дневное стандартное отклонение
    std_long = pd.rolling_std(difference, window=long)
    std_long.name = 'std {0}d'.format(long)

    # Рассчитываем z-оценку на каждый день
    zscore = (diff_mavg_short - diff_mavg_long)/std_long
    zscore.name = 'z-ma {0}d/{1}d'.format(short, long)

    return zscore

def get_performance(a):
    """
    Convert vector to performance
    """
    a = a.round(2)
    return np.insert(np.cumsum(np.diff(a) / a[:-1] * 100.), 0, 0)

def get_diff(y):
    # Метод наименьших квадратов
    x = np.array([i for i in range(len(y))])
    arr = np.vstack([x, np.ones(len(x))]).T
    m, c = np.linalg.lstsq(arr, y)[0]
    #print(m, c)
    return c